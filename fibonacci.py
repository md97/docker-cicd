from test_fibonacci import num

print(f"Let's calculate fibonacci sequence up to {num}th")

def fib(n):
    # 0 1 1 2 3 5 8 13 21
    x = 0
    y = 1

    for r in range(n):
        z = y+x
        x = y
        y = z

        print(x)
        


fib(num)