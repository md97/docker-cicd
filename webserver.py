import http.server
import socketserver

port = 8080
Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", port), Handler) as httpd:
    print(f"serving at port {port}")
    httpd.handle_request()